const pg_1 = require("pg");

async function pgCon(query) {
    const conProps = {
        user: "iknoplus",
        host: "127.0.0.1",
        database: "iknoplus",
        password: "iknoplus",
        port: 5432
    };
    const client = new pg_1.Client(conProps);
    client.connect();
    let result = await client.query(query);
    client.end();
    return result;
}
exports.pgCon = pgCon;
test();
async function test(){
	console.log(await pgCon('select * from batch_instance'))
}